/*
* @file
* Provides client side functions to aid in the display of form fields.
*/

(function ($) {


	// Set form based on selected student type
	function setup_student_type(student_type) {
		switch(student_type){
			case "high-school-student":
				$("#international").hide();
				$("#canadian").show();
				$("#canadian .section").show();
				$("#programs").show();
				$("#nonTypicalStudentTypes").hide();
				break;
			case "other-student":
				$("#international").hide();
				$("#canadian").hide();
				$("#canadian .section").hide();
				$("#programs").hide();
				$("#nonTypicalStudentTypes").show();
				break;
			case "college-transfer":
				$("#canadian").show();
				$("#canadian .section").hide();
				$("#programs").show();
				$("#international").hide();
				$("#nonTypicalStudentTypes").hide();
				break;
			case "university-transfer":
				$("#canadian").show();
				$("#canadian .section").hide();
				$("#programs").show();
				$("#international").hide();
				$("#nonTypicalStudentTypes").hide();
				break;
		}
	}

	// Set visibility of international countries
	function set_international_countries(system) {
		switch(system){
			case "international":
				$(".form-item-country").show();
				break;
			default:
				$(".form-item-country").hide();
		}
	}

	// Set visibility of domestic/international systems
	function set_systems(province) {
		switch(province){
				case "other":
					$("#international").show();
					$(".form-item-country").hide();
					$("#canadian .section").hide();
					break;
				default:
					$("#international").hide();
			}
	}

	function update_form() {
		setup_student_type($("#edit-studenttype").val());
		set_systems($("#edit-province").val());
		set_international_countries($("#edit-system").val());
	}


	$("document").ready(function() {

		//Initialize form
		update_form();

		//Updates form by checking all dropdown options
		$("#edit-studenttype, #edit-province, #edit-system").change(function (){
			update_form();
		});

    $("input:checkbox").click(function(){
      var group = "input:checkbox[class='"+$(this).prop("class")+"']";
      if ($(this).prop('checked') == true) {
        $(group).prop("checked",false);
        $(this).prop("checked",true);
      } else {
        $(this).prop("checked", false);
      }
    });
	});

})(jQuery);
