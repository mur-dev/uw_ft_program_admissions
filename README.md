# UW Program Admissions Module
This module allows a user to search for admission level programs. 

[https://uwaterloo.ca/future-students/admissions/admission-requirements](https://uwaterloo.ca/future-students/admissions/admission-requirements)

---
---
# Functions

---
---
#### function uw_ft_program_admissions_menu() 
Define the field types for the search form

##### Returns
An associative array of the displayed results

##### Additional Information
See the [api documentation](https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu/7.x) for this specific hook for more information.

---
---
#### function uw_ft_program_admissions_field_info()
Provides the description of the field.

##### Returns
An associative array with default properties.

##### Additional Information
See the [api documentation](https://api.drupal.org/api/drupal/modules%21field%21field.api.php/function/hook_field_info/7.x) for this specific hook for more information.

---
---

#### function uw_ft_program_admissions_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors)
Allows us to valide the form settings. 

##### Additional Information
See the [api documentation](https://api.drupal.org/api/drupal/modules%21field%21field.api.php/function/hook_field_validate/7.x) for this specific hook for more information.

#### function uw_ft_program_admissions_field_is_empty($item, $field) 
Check if the field is empty. 

##### Additional Information
See the [api documentation](https://api.drupal.org/api/drupal/modules!field!field.api.php/function/hook_field_is_empty/7.x) for this specific hook for more information.

---
---
#### function uw_ft_program_admissions_field_formatter_info()
Declares that we will be using a custom view for the field, as a form.

##### Returns
An associative array of the default properties.

##### Additional Information
See the [api](https://api.drupal.org/api/drupal/modules%21field%21field.api.php/function/hook_field_formatter_info/7.x) for this specific hook for more information.

---
---
#### function uw_ft_program_admissions_results($form, $form_state)
The result page of the form; displaying all the programs that match the keyword.

##### Returns
A associative array containing search word keywords.

---
---
#### function uw_ft_program_admissions_display($form, $form_state, $item)
Initial landing page for the content type.

##### Returns
An associative array contain user inputted information.

---
---
#### function uw_ft_program_admissions_display_submit($form, &$form_state)
Submits form by passing the field in the URL.

---
---
#### function uw_ft_program_admissions_get_provinces()
Retrieves a list of provinces from the database.

##### Returns
An array containing the provinces.

---
---
#### function uw_ft_program_admissions_get_systems
Retrieves a list of systems.

##### Returns
An array of systems.

---
---
#### function uw_ft_program_admissions_get_countries()
Retrieves a list of countries.

##### Returns
An array of countries.

#### function uw_ft_program_admissions_programs_query($program)
Grabs the program name, recommended courses, average required, notes, and info links.

##### Returns
An array containing admission requirements.

---
---
#### function uw_ft_program_admissions_university_transfer_query($program)
Grabs the university transfer requirements

##### Returns
An array containing university transfer requirements.

---
---
#### function uw_ft_program_admissions_college_transfer_query($program)
Grabs the required courses, required average, transfer credits and any additional notes for college transfer requriements.

##### Returns
An array containing college transfer requirements.

---
---
#### function uw_ft_program_admissions_domestic_province_query($province) 
Grabs the province name and general requirements.

##### Returns
An array containing domestic province systems.

---
---
#### function uw_ft_program_admissions_domestic_course_requirements($province_id, $program_id)
Grabs the required courses.

#### Returns 
An array containing domestic course requirments.

---
---
#### function uw_ft_program_admissions_domestic_course_requirements_groups($province_id, $program_id)
Grabs the course requirements groups for a specific program and province.

##### Returns
An array containing groups of domestic requirments.

---
---
#### function uw_ft_program_admissions_domestic_course_groups($province_id, $program_id)
Grabs the course for each requirements group for a specific program and province.

##### Returns
An array containing group of course domestic requirements.

---
---
#### function uw_ft_program_admissions_international_systems_query($system)
Grabs the system name and general requirements for that system.

##### Returns
An array containing international systems and general requirements for that system.

---
---
#### function uw_ft_program_admissions_international_systems_program_query($system_id, $program_id)
Grabs the actual requirements for a system give a specific program.

##### Returns
An array containing actual international requirements.

---
---
#### function uw_ft_program_admissions_international_country_query($country)
Grabs the country name and general requirements for a given country ID.

##### Returns
An array containing countries name and general requirements.

---
---
#### function uw_ft_program_admissions_display_req_courses($admissions_course_result)
Displays the required course.

##### Returns
An array of HTML tags.

---
---
#### function uw_ft_program_admissions_override($system, $program, &$output)
This overrides general sections for more specific requirements, such as notes, general requirements, and program requirements.

---
---
#### function uw_ft_program_admissions_callback($program, $student_type, $system)
Displays the actual admissions requirements.

##### Returns
An array containing HTML tags and program informations.

---
---
#### function uw_ft_program_admissions_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display)
Calls form as the display for the form.

##### Returns
An array of elements.

##### Additional Information
See the [api documentation](https://api.drupal.org/api/drupal/modules%21field%21field.api.php/function/hook_field_formatter_view/7.x) for this specific hook for more information.

---
---
#### function uw_ft_program_admissions_field_widget_info()
Declare the form as a widget.

##### Returns
An associative array containing form descriptions.

---
---
#### function uw_ft_program_admissions_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element)
Creates elements from the widget form.

##### Returns
An array of elements from the widget.

---
---
#### function uw_ft_program_admissions_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items
Text_format returns an array which messes up the DB insert.

---
---
#### uw_ft_program_admissions_widget_error($element, $error, $form, &$form_state)
Raises an error when there is an invalid input.

